/*
package net.coremotion.challenge1.ui.base

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding

typealias Inflate<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<BD : ViewBinding>(private val inflate: Inflate<BD>) : Fragment() {

    private var _binding: BD? = null
    protected val binding: BD get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflate.invoke(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        start()
        _binding = null
    }

    abstract fun start(
    )
}
*/
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding


typealias Inflater <RD> = (LayoutInflater, ViewGroup?, Boolean) -> RD
abstract class BaseFragment <VB : ViewBinding> (var inflate: Inflater<VB>) : Fragment() {

    private var _binding: VB? = null
    open val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding=inflate.invoke(inflater,container,false)
        start()
        return binding.root
    }
    abstract fun start()
    override fun onDestroyView() {
        super.onDestroyView()
        _binding=null
    }

}